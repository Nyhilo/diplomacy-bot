import os

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service

# Setup the selenium Chrome driver
_options = Options()
_options.add_argument("--headless")  # Ensure GUI is off
_options.add_argument("--no-sandbox")
_options.add_argument("window-size=1920x1080")

_service = Service('./chromedriver/stable/chromedriver')
_driver = webdriver.Chrome(service=_service, options=_options)


# TODO: bring all this junk in via config
URL_BASE = 'https://www.backstabbr.com/game/Nomic--Friends-Season-2/4913831610155008'
PARENT_DIR = 'maps'
MAP_FILE_BASE = 'map'
MAP_FILE_EXTENSION = 'png'


def get_map(year, season):
    '''
    Fetches the configured webpage and takes a screenshot of the element on the page with the id "#map"    
    '''

    url = f'{URL_BASE}/{year}/{season}'
    print(f'Getting map from {url}')
    _driver.get(url)

    filename = f'./{PARENT_DIR}/{MAP_FILE_BASE}_{year}_{season}.{MAP_FILE_EXTENSION}'

    element = _driver.find_element(By.ID, 'map')

    if not os.path.exists(f'./{PARENT_DIR}'):
        os.makedirs(f'./{PARENT_DIR}')

    element.screenshot(filename)

    return filename


if __name__ == '__main__':
    from sys import argv

    if len(argv) < 3 or len(argv) > 3:
        exit(f'Usage:\n\t{argv[0]} [year] [season]\n'
             '\tDownload a screenshot of the map pertaining to the given year and season.\n'
             '\tReturns the filename of the retrieved screenshot')

    year, season = argv[1], argv[2]

    print('Retrieving map...')

    filename = get_map(year, season)

    print(f'Screenshot saved as {filename}')
