import typing
import random
import json
from unicodedata import category

import discord
from discord.ext import commands

intents = discord.Intents.default()
intents.members = True

bot = commands.Bot(command_prefix="!", description="This bot helps with creating private channels for discussion between countries.", intents=intents)

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')

with open("config.json") as cfg_file:
    try:
        config = json.load(cfg_file)
        print("Successfully loaded configuration.")
    except:
        print("Failed to load configuration.")

with open("game.json") as game_file:
    try:
        game = json.load(game_file)
        print("Successfully loaded game data.")
    except:
        print("Failed to load game data.")

def create_overwrites(ctx, *objects):
    """This is just a helper function that creates the overwrites for the 
    voice/text channels.
    A `discord.PermissionOverwrite` allows you to determine the permissions
    of an object, whether it be a `discord.Role` or a `discord.Member`.
    In this case, the `view_channel` permission is being used to hide the channel
    from being viewed by whoever does not meet the criteria, thus creating a
    secret channel.
    """

    # a dict comprehension is being utilised here to set the same permission overwrites
    # for each `discord.Role` or `discord.Member`.
    overwrites = {
        obj: discord.PermissionOverwrite(view_channel=True)
        for obj in objects
    }

    for country in game["dead_countries"]:
        role_id = config["countries"][country]["role"]
        role = ctx.guild.get_role(role_id)
        overwrites[role] = discord.PermissionOverwrite(send_messages=False)

    # prevents the default role (@everyone) from viewing the channel
    # if it isn't already allowed to view the channel.
    overwrites.setdefault(ctx.guild.default_role, discord.PermissionOverwrite(view_channel=False))

    # makes sure the client is always allowed to view the channel.
    overwrites[ctx.guild.me] = discord.PermissionOverwrite(view_channel=True)

    return overwrites

def get_country_name_from_channel_id(id):
    for name, country in config["countries"].items():
        if country["channel"] == id:
            return name
    else:
        raise Exception(f"Channel id {id} is not associated with a country.")

def save_game():
    with open("game.json", "w") as game_file:
        try:
            json.dump(game, game_file)
        except:
            print("Failed to save game data.")


@bot.command()
async def roll(ctx, dice: str):
    """Rolls a dice in NdN format."""
    try:
        rolls, limit = map(int, dice.split('d'))
    except Exception:
        await ctx.send('Format has to be in NdN!')
        return

    result = ', '.join(str(random.randint(1, limit)) for r in range(rolls))
    await ctx.send(result)

@bot.command()
@commands.guild_only()
async def create_channel(ctx: commands.Context, *names: str):
    """Creates an international discussion channel with the specified country/countries."""
    if ctx.channel.category == None:
        await ctx.send("This command can only be used in one of the country chats.")
        return
    
    if ctx.channel.category.id != config["categories"]["country-chat"]:
        await ctx.send("This command can only be used in one of the country chats.")
        return

    this_country_name = get_country_name_from_channel_id(ctx.channel.id)

    if this_country_name in game["dead_countries"]:
        await ctx.send("This country is dead! Dead countries cannot create channels.")

    country_names_unsorted = set()
    country_names_unsorted.add(this_country_name)

    for name in names:
        for country_name, country in config["countries"].items():
            if name.lower() in country["names"]:
                country_names_unsorted.add(country_name)
                break
        else:
            await ctx.send(f"Invalid country name {name}. Attempting to continue anyways.")

    country_names = sorted(country_names_unsorted)

    if len(country_names) < 2:
        await ctx.send("Not enough valid countries.")
        return

    channel_name = ""
    for country_name in country_names:
        channel_name += config["countries"][country_name]["names"][2] # third name always is a single letter

    if channel_name in game["channels"].keys():
        await ctx.send("Channel already exists.")
        return

    c_names_cap = [name.capitalize() for name in country_names]

    topic = "Discussion for "
    if len(country_names) == 2:
        topic += f"{c_names_cap[0]} and {c_names_cap[1]}."
    else:
        topic += f"{', '.join(c_names_cap[:-1])}, and {c_names_cap[-1]}."

    country_roles = [ctx.guild.get_role(config["countries"][country]["role"]) for country in country_names]

    overwrites = create_overwrites(ctx, *country_roles)

    for cat in ctx.guild.categories:
        if cat.id == config["categories"]["international-discussion"]:
            id_category = cat

    channel = await ctx.guild.create_text_channel(
        channel_name,
        overwrites=overwrites,
        category=id_category,
        topic=f"{topic}",
        reason=f"Created {channel_name}.",
    )

    await ctx.send(f"Created {channel.mention}.")

    game["channels"][channel_name] = channel.id

    save_game()

@bot.command()
@commands.guild_only()
async def rename_channel(ctx: commands.Context, *words: str):
    """Renames an international discussion channel."""
    if ctx.channel.category == None:
        await ctx.send("This command can only be used in an international discussion channel.")
        return

    if ctx.channel.category.id != config["categories"]["international-discussion"]:
        await ctx.send("This command can only be used in one of the country chats.")
        return

    channel_id = ctx.channel.id

    channels_by_id = dict((id, countries) for countries, id in game["channels"].items())

    name = "-".join(words).lower()

    await ctx.channel.edit(name=f"{channels_by_id[channel_id]}-{name}")

    await ctx.send("Renamed the channel.")

#unsubmitted_role_name = "Orders Not Submitted"

""" @bot.command()
@commands.guild_only()
async def orders(ctx: commands.Context):
    \"""Toggles whether your country has submitted orders.\"""
    if ctx.channel.category.name != "Country Chats":
        await ctx.send("This command can only be used in one of the country chats.")
        return

    country = ctx.channel.name

    for role in ctx.guild.roles:
        if role.name == unsubmitted_role_name:
            unsubmitted_role = role
            break
    else:
        await ctx.send("Couldn't find the 'orders not submitted' role - did it get deleted somehow?")
        return

    for role in ctx.guild.roles:
        if role.name == country.capitalize():
            country_role = role
            break
    else:
        await ctx.send("Couldn't find this country's role - is this country dead?")
        return

    country_members = []

    async for member in ctx.guild.fetch_members(limit=100):
        if country_role in member.roles:
            country_members.append(member)

    if unsubmitted_role in country_members[0].roles:
        submitted = False
    else:
        submitted = True

    for member in country_members:
        if submitted:
            await member.add_roles(unsubmitted_role, reason="'!orders' command")
        else:
            await member.remove_roles(unsubmitted_role, reason="'!orders' command")

    if submitted:
        await ctx.send("Marked this country's orders as being unsubmitted.")
    else:
        await ctx.send("Marked this country's orders as being submitted.")

game_admin_role_name = "Game Admin"

@bot.command()
@commands.guild_only()
async def adjudicate(ctx: commands.Context):
    \"""Tells the bot that the game has adjudicated, giving all countries the 'orders not submitted' role. This command is admin-only.\"""

    if ctx.author.top_role.name != game_admin_role_name:
        await ctx.send("This command can only be used by a game admin.")
        return

    for role in ctx.guild.roles:
        if role.name == unsubmitted_role_name:
            unsubmitted_role = role
            break
    else:
        await ctx.send("Couldn't find the 'orders not submitted' role - did it get deleted somehow?")
        return
    
    for role in ctx.guild.roles:
        if role.name in valid_country_names:
            for member in role.members:
                await member.add_roles(unsubmitted_role, reason="'!adjudicate' command")

    await ctx.send("Marked all countries' orders as being unsubmitted.")
 """

with open("key.txt") as keyf:
    key = keyf.read()

bot.run(key)